// console.log("Hello World");

/*let printFriends = function(){
   let friend1 = prompt("Enter your full name: "); 
   console.log(friend1); 
};
printFriends();*/



function printName(name){
   console.log("Hello " + name);
}
printName("Joana");
//both arguments
printName("John");
printName("Jane");

let sampleVar = "Yua";

printName(sampleVar);


function printName3(num1, num2){
   console.log("The number passed as argument are: ");
   console.log(num1);
   console.log(num2);
}
printName3(15, 30);


function printName2(name1, name2, name3){
   console.log("My Friends are " + name1 + ", " + name2 + " and " + name3);
}
printName2("John","Jane", "Joana");


function checkDivisibilityBy8(num){
   let remainder = num%8;
   console.log("the remainder of " + num + " is " + remainder);
   let isDivisibleBy8 = remainder ===0;
   console.log("Is " + num + " divisible by 8?");
   console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);


function checkDivisibilityBy4(num5){
   let remainder1 = num5%4;
   console.log("the remainder of " + num5 + " is " + remainder1);
   let isDivisibleBy4 = remainder1 ===0;
   console.log("Is " + num5 + " divisible by 4?");
   console.log(isDivisibleBy4);
}
checkDivisibilityBy4(64);
checkDivisibilityBy4(28);


function isEven(num){
   console.log(num % 2 ===0);
}

function isOdd(num1){
   console.log(num1 % 2 !==0);
}
/*let numEven = isEven(20);
let numOdd = isOdd(31);*/

isEven(20);
isOdd(42);

//FUNCTION AS ARGUMENTS
function argumentFunction(){
   console.log("This function is passed into another function.");
}

function invokeFunction(functionParameter){
   console.log("This is invoke Function");
   functionParameter();
}
invokeFunction(argumentFunction);



/*function printFullName(firstName,middleName,lastName){
   console.log(firstName + " " + middleName + " " + lastName);
}
printFullName("Aaron", "Song", "Concepcion");*/

/*let firstName = "Juan";
let middleName = "Dela";
let lastName = "Cruz";

console.log(firstName + " " + middleName + " " + lastName);*/

//Return Statement
//return statement allows us to output a value from a function to passed to the line/block od code that invoked/called our function
function returnFullName(firstName,middleName,lastName){
   return firstName + " " + middleName + " " + lastName;
   // console.log("This message will not be printed");
}
console.log(returnFullName("Jeffrey", "Smith", "Bezos"));



let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);


function returnAddress(address1,address2){
   let fullAddress = address1 + ", " + address2;
   return fullAddress;
   
}
let myAddress = returnAddress("Canlubang ", "Calamba");
console.log(myAddress);

console.log(returnAddress("Batangas ", "City"));



function printPlayerInfo(username, level, job){
   console.log("Username: " + username);
   console.log("Level: " + level);
   console.log("Job: " +job);
}
let user1 = printPlayerInfo("knight_white", 95, "Paladin");

//returns undefined because printPlayerInfo return nothing. it's task to print the messages in the console, but not to return any value.
console.log(user1);